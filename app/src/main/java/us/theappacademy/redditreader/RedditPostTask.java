package us.theappacademy.redditreader;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class RedditPostTask extends AsyncTask<RedditListFragment, Void, JSONObject>{
    private RedditListFragment redditListFragment;

    @Override
    protected JSONObject doInBackground(RedditListFragment... redditListFragments) {
        JSONObject jsonObject = null;
        redditListFragment = redditListFragments[0];

        try {
            URL redditFeedUrl = new URL("https://reddit.com/r/android.json");

            HttpURLConnection httpConnection = (HttpURLConnection)redditFeedUrl.openConnection();
            httpConnection.connect();

            int statusCode = httpConnection.getResponseCode();

            if(statusCode == HttpURLConnection.HTTP_OK) {
                jsonObject = RedditPostParser.getInstance().parseInputStream(httpConnection.getInputStream());
            }
        }
        catch(MalformedURLException error) {
            Log.e("RedditPostTask", "MalformedURLException (doInBackground): " + error);
        }
        catch(IOException error) {
            Log.e("RedditPostTask", "IOException (doInBackground): " + error);
        }

        return jsonObject;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        RedditPostParser.getInstance().readRedditFeed(jsonObject);
        redditListFragment.updateUserInterface();
    }
}
